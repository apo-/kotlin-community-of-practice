# Kotlin Community of Practice

Material for a Kotlin Community of Practice.

## Contents

* Null Safety
* Release Notes _(coming soon)_
* Patch Notes _(coming soon)_
* TIL (Today I Learned) _(coming soon)_
* How I Built This _(coming soon)_
* Decode Kotlin _(coming soon)_
* Can Kotlin Do It Better? _(coming soon)_

## Resources

* [Kotlin Reference][1]
* [Kotlin Playground][2]
* [Learn X in Y minutes where X=Kotlin][3]

## Authors

* Alex Ordonez

[1]: https://kotlinlang.org/docs/reference/
[2]: https://play.kotlinlang.org
[3]: https://learnxinyminutes.com/docs/kotlin/
