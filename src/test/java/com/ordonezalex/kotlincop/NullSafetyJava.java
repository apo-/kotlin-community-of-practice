package com.ordonezalex.kotlincop;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Map;
import org.junit.jupiter.api.Test;

/**
 * @see NullSafety
 */
class NullSafetyJava {

  @Test
  void aLotOfNullChecks() {
    Map<String, Map<String, Map<String, Map<String, Map<String, Map<String, String>>>>>> nested;
    nested = Map.of("level-1", Map.of("level-2", Map.of("level-3", Map.of("level-4", Map.of("level-5", Map.of("level-6", "nested"))))));

    String result = "not found";
    Map<String, Map<String, Map<String, Map<String, Map<String, String>>>>> level1 = nested.get("level-1");
    if (level1 != null) {
      Map<String, Map<String, Map<String, Map<String, String>>>> level2 = level1.get("level-2");
      if (level2 != null) {
        Map<String, Map<String, Map<String, String>>> level3 = level2.get("level-3");
        if (level3 != null) {
          Map<String, Map<String, String>> level4 = level3.get("level-4");
          if (level4 != null) {
            Map<String, String> level5 = level4.get("level-5");
            if (level5 != null) {
              String level6 = level5.get("level-6");
              if (level6 != null) {
                result = level6;
              }
            }
          }
        }
      }
    }

    System.out.println("The result is " + result);
    assertEquals("nested", result, "The result is incorrect");
  }

  @Test
  void aLotOfHiddenNullChecks() {
    Map<String, Map<String, Map<String, Map<String, Map<String, Map<String, String>>>>>> nested;
    nested = Map.of("level-1", Map.of("level-2", Map.of("level-3", Map.of("level-4", Map.of("level-5", Map.of("level-6", "nested"))))));

    String result = "not found";
    try {
      result = nested.get("level-1").get("level-2").get("level-3").get("level-4").get("level-5").get("level-6");
    } catch (NullPointerException ignored) {
    }

    System.out.println("The result is " + result);
    assertEquals("nested", result, "The result is incorrect");
  }
}
