package com.ordonezalex.kotlincop

import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals

/**
 * See the official reference: https://kotlinlang.org/docs/reference/null-safety.html
 */
class NullSafety {

    @Test
    fun `Reassign String As Null`() {
        var a: String = "Do I compile?"

//        a = null

//        val l = a.length
    }

    @Test
    fun `Reassign String? As Null`() {
        var a: String? = "Do I compile?"

//        a = null
//        println(a)

//        val l = a.length
    }

    fun `Test for null`() {
        val a: String? = "How do I check for null?"

//        val l = if (a != null) a.length else -1

//        val l = a?.length ?: -1
    }

    fun `Now that we know a is not null`() {
        val a: String? = "Does this mean that I need to put null-checks everywhere?"

//        if (a != null && a.length > 0) {
//            println("String of length ${a.length}")
//        } else {
//            println("Empty string!")
//        }

    }

    /**
     * @see com.ordonezalex.kotlincop.NullSafetyJava.aLotOfNullChecks
     */
    @Test
    fun `A Lot Of Null Checks`() {
        val nested: Map<String, Map<String, Map<String, Map<String, Map<String, Map<String, String>>>>>>
        nested = mapOf("level-1" to mapOf("level-2" to mapOf("level-3" to mapOf("level-4" to mapOf("level-5" to mapOf("level-6" to "nested"))))))

//        var result = "not found"
//        val tmp = nested["level-1"].get("level-2").get("level-3").get("level-4").get("level-5").get("level-6")
//        if (tmp != null) {
//            result = tmp
//        }

//        val result = nested["level-1"]?.get("level-2")?.get("level-3")?.get("level-4")?.get("level-5")?.get("level-6") ?: "not found"

//        println("The result is $result")
//        assertEquals("nested", result, "The result is incorrect")
    }

    @Test
    fun `Unnecessarily Safe`() {
        val a = "Never null"
        val b: String? = null

//        println(b?.length)
//        println(a?.length)
    }

    @Test
    fun `Actually, I need NullPointerException in my life`() {
        data class Node(var parent: Node?, var name: String?)

        fun foo(node: Node): String? {
            // We can return null
            val parent = node.parent ?: return null

            // We can throw an exception
            return parent.name ?: throw IllegalArgumentException("name expected")
        }

        val root = Node(null, "Root")
//        foo(root)

        val child = Node(root, "Child")
//        foo(child)
    }

    @Test
    fun `I want more null!!`() {
        val b: String? = if (0 == Random.nextInt(0, 6)) null else "Lucky String"

        val l = b!!.length
    }

    @Test
    fun `Easily filter out nulls`() {
        val mixedNulls = listOf("Not null, null,","null",",",",",",",",",null,"null")

        assertEquals(8, mixedNulls.size, "Size is incorrect")
    }

}
